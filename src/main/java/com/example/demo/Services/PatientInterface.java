package com.example.demo.Services;

import com.example.demo.Model.Patient;

import java.util.List;

public interface PatientInterface {

abstract  List<Patient> getAllPatients();
abstract  Patient getSpacificPatient(Integer id);
abstract  boolean  addPatient(Patient patient);
abstract  boolean deletePatient(Integer id);
abstract  boolean updatePatient(Patient patient, Integer id);

}




