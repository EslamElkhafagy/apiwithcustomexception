package com.example.demo.Model;

import javax.persistence.*;
import java.util.List;


@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String age;
    private String address;

//    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
//    @JsonIgnore
//    private List<Disease> diseases;

    public Patient() {

    }

    public Patient(Integer id, String name, String age, String address/*, List<Disease> diseases*/) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
//        this.setDiseases(diseases);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public List<Disease> getDiseases() {
//        return diseases;
//    }

//    public void setDiseases(List<Disease> diseases) {
//        this.diseases = diseases;
//    }
}// end class
