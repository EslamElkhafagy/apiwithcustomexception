package com.example.demo.Model;


import javax.persistence.*;

@Entity
public class Disease {


@Id
@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String diseaseName;
    private String diseaseDegree;

//    @ManyToOne
//    @JoinColumn(name = "patient_id")
//    private Patient patient;

    public Disease() {
    }

    public Disease(Integer id, String diseaseName, String diseaseDegree) {
        this.id = id;
        this.diseaseName = diseaseName;
        this.diseaseDegree = diseaseDegree;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseDegree() {
        return diseaseDegree;
    }

    public void setDiseaseDegree(String diseaseDegree) {
        this.diseaseDegree = diseaseDegree;
    }

//    public Patient getPatient() {
//        return patient;
//    }

//    public void setPatient(Patient patient) {
//        this.patient = patient;
//    }
}
