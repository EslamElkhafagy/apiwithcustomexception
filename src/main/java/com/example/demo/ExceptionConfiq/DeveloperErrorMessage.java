package com.example.demo.ExceptionConfiq;


import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

public class DeveloperErrorMessage {

    private HttpStatus statusCode;
    private String errorMessage;
    private List<String> errors;




    public DeveloperErrorMessage(HttpStatus statusCode, String errorMessage, List<String> errors) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        this.errors = errors;
    }

    public DeveloperErrorMessage(HttpStatus statusCode, String errorMessage, String errors) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        this.errors = Arrays.asList(errors);
    }


    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
