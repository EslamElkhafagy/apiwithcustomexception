package com.example.demo.ExceptionConfiq;


import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    public CustomExceptionHandler() {
        super();
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
// show data on logger
        logger.info(ex.getClass().getName());

// customize method message
        StringBuffer buffer = new StringBuffer();
        buffer.append(ex.getMethod());
        buffer.append("  Method is Not Supported for This Request . Should be ");
        ex.getSupportedHttpMethods().forEach(s -> buffer.append(s + " "));

// add this Error data in MY DeveloperErrorMessage
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.METHOD_NOT_ALLOWED, ex.getLocalizedMessage(), buffer.toString());


        return new ResponseEntity<>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());

    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
// show data on logger
        logger.info(ex.getClass().getName());

// customize method message
        StringBuffer buffer = new StringBuffer();
        buffer.append(ex.getContentType());
        buffer.append("  Media Type is Not Supported for This Request . Should be ");
        ex.getSupportedMediaTypes().forEach(s -> buffer.append(s + " "));

// add this Error data in MY DeveloperErrorMessage
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex.getLocalizedMessage(), buffer.toString());


        return new ResponseEntity<>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        // show data on logger
        logger.info(ex.getClass().getName());

// customize method message
        List<String> errors = new ArrayList<>(); 
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
// add this Error data in MY DeveloperErrorMessage
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);


        return new ResponseEntity<>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());

    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType();

        // add this Error data in MY DeveloperErrorMessage
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);


        return new ResponseEntity<>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());

    }


    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        String error = ex.getParameterName() + " parameter is missing";
        // add this Error data in MY DeveloperErrorMessage
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);


        return new ResponseEntity<>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());

    }


    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());
    }


    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //
        DeveloperErrorMessage developerErrorMessage = new DeveloperErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), "error occurred");
        return new ResponseEntity<Object>(developerErrorMessage, new HttpHeaders(), developerErrorMessage.getStatusCode());
    }

}
