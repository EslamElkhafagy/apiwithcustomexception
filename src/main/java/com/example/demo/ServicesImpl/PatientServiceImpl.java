package com.example.demo.ServicesImpl;

import com.example.demo.Model.Disease;
import com.example.demo.Model.Patient;
import com.example.demo.Repository.PatientRepo;
import com.example.demo.Services.PatientInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientInterface {
//    List<Patient> patients = new ArrayList<Patient>();
//
//    List<Disease> diseases = Arrays.asList(new Disease(1, "Stomic Upset", "Normal"),
//            new Disease(2, "cold", "High"));
//@Bean
//     public void loadData(){
//
//
//         patients.add(new Patient(1, "Eslam", "23", "Egypt"/*, diseases*/));
//                 patients.add(new Patient(2, "Ahmed", "58", "Egypt"/*, diseases*/));
//
//     }
    @Autowired
    PatientRepo patientRepo;

    @Override
    public List<Patient> getAllPatients() {


        return patientRepo.findAll();
    }

    @Override
    public Patient getSpacificPatient(Integer id) {
        return patientRepo.findById(id).get();
    }

    @Override
    public boolean addPatient(Patient patient) {
        System.out.println(patient.toString());

         patientRepo.save(patient);
        return true;
    }

    @Override
    public boolean deletePatient(Integer id) {
        try {
            patientRepo.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean updatePatient(Patient patient, Integer id) {



//        Patient p = patientRepo.findById(id).get();
        patientRepo.save(patient);
        return true;
    }
}
