package com.example.demo.Controller;


import com.example.demo.Model.Patient;
import com.example.demo.Services.PatientInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    PatientInterface patientInterface;

/*
* http://localhost:8080/patient/all
* */
@RequestMapping(value = "/all", method = RequestMethod.GET)
public List<Patient> getAllPatients(){

return patientInterface.getAllPatients();
}


/*
* http://localhost:8080/patient/get/{id}
* */

@RequestMapping(value="/get/{patient_id}",method = RequestMethod.GET)
public Patient getSpacificPatient(@PathVariable(value = "patient_id") Integer patient_id){

return patientInterface.getSpacificPatient(patient_id);
}

@RequestMapping(value = "/add", method = RequestMethod.POST)
public boolean addPatient(@RequestBody Patient patient){
System.out.println(patient.toString());
    return patientInterface.addPatient(patient);
}


@RequestMapping(value = "/delete",method = RequestMethod.DELETE)
public boolean deletePatient(@RequestParam("patient_id") Integer id){

    return patientInterface.deletePatient(id);
}

@RequestMapping(value = "/update/{patient_id}")
public boolean   updatePatient(@RequestBody Patient patient,@PathVariable(value = "patient_id") Integer id){

return patientInterface.updatePatient(patient,id);
}


}
