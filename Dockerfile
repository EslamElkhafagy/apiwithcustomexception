FROM openjdk:8
ADD target/Api-Revision.jar Api-Revision.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","Api-Revision.jar"]